const util = require("rmm-nodejs-package-util")

console.log(
    util.isNumber('1'), // false
    util.isNumber(1) // true
)

console.log(
    util.isBoolean(true), // true
    util.isBoolean("true"), // false
    util.isBoolean(false), // true
    util.isBoolean(0), // false
    util.isBoolean(1) // false
    )

    console.log(
        util.strictBool('true'), // true
        util.strictBool('false'), // false
        util.strictBool('yes'), // true
        util.strictBool('no') // false
    );