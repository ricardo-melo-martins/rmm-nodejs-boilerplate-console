(function () {
  "use strict";

  const yargs = require('yargs/yargs')
  const { hideBin } = require('yargs/helpers')
  const args = yargs(hideBin(process.argv))

  const { spawn } = require('child_process')
  const path = require('path')
  const isEmpty = require('./libs/is-empty');
  
  const packageInfo = require('../package.json')
  var VERSION = require(path.join(__dirname, '..', 'package.json')).version;
  
  var info = {
    clientType: 'cli',
    clientVersion: VERSION
  };

  
  var argv = yargs(hideBin(process.argv))
  .scriptName("rmm")  
  .usage('Usage: $0 <cmd> [args]')
  .command('info [command]', 'Application Info', (yargs) => {
      
        yargs.positional('command', {
            type: 'string',
            default: 'info',
            describe: 'Por favor, forneça um nome de comando',
            alias: ('-i', 'info')
        })

    }, (argv) => {

        let isVerbose = !!argv.verbose;
        if (isVerbose) console.log('Call', argv.command, 'Task proccess!')
      
    })
    .command('new [command]', 'Application Create', (yargs) => {
      
        yargs.positional('command', {
            type: 'string',
            default: 'app',
            describe: 'Por favor, forneça um nome de comando',
            alias: ('-a', 'app')
        });
        
        yargs.option('file', {
            alias: 'f',
            demandOption: false,
            describe: 'Path to a tflite model',
            type: 'string',
            conflicts: 'automl'
          });

    }, (argv) => {

        let isVerbose = !!argv.verbose;
        if (isVerbose) console.log('Call', argv.command, 'Task proccess!')
      
    })
    .option('verbose', {
      alias: 'v',
      type: 'boolean',
      description: 'Apresenta log'
    })
    .demandCommand()
    .help()
    .parse()
    

        
        async function exec(...args) {
            const proc = spawn(...args)
            await new Promise(res => proc.on('close', res))
        }

        async function execTask(task, options) {
            var pathJoin = path.join(`${__dirname}\\tasks`, `${task.set}.js`)
            
            var result = (require(pathJoin)).run(task.name, info, options);
            
             Promise.resolve(result).then(function(result) {
                if (result && result.nextTask) {
                    execTask(result.nextTask);
                }
            })
            
        }
        
  var Rmm = {

    evaluateTask: function(_args) {
        
        const isVersion = !!_args.v;
        const isInfo = !!_args.info || !!_args.i;
        const isApp = !!_args.app || !!_args.a;
              
        let task = {}
        
        if(isInfo){
            task.name = "info"
            task.set = "info"
        }
        
        if(isVersion){
            task.name = "version"
            task.set = "version"
        }

        if(isApp){
            task.name = "app"
            task.set = "app"
        }

        return task
    },

    handleArguments: function(_args) {
        
        console.dir(_args);
        
        let options = {}
        // todo: parsing and types validate
        options = this.evaluateTask(_args)
        
        return options
    },

    run: async function () {

        console.info(`Running from: ${process.cwd()}`);

        try {
            
            let args = this.handleArguments(argv)
            console.log('args', args);
            let task = args
            let taskName = task.name

            console.info(`Call ${taskName} Task`);
            
            if(!isEmpty(task)){
                await execTask(task)
            }
            
        } catch (error) {
            console.error(error)
            process.exit(0);
        }

        process.exit(0); 
    },
  };

  process.on("exit", function () {
    console.log("exit...");
  });

  process.on("SIGINT", function () {
    console.log("SIGINT...");
    process.exit();
  });

  exports.Rmm = Rmm;
})();
