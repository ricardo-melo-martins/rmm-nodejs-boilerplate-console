

  var InfoTask = {};
  
  InfoTask.showInfo = function(taskName, info, options) {
    console.log(`run ${taskName} task`, info, options)
  }

  InfoTask.run = function(taskName, info, options) {
    
    this.showInfo(taskName, info, options);
  };

  module.exports = InfoTask;   