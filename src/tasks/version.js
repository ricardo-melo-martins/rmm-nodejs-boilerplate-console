
  var VersionTask = {};
  
  VersionTask.showVersion = function(taskName, info, options) {
    console.log(`run ${taskName} task`, info, options)
  }

  VersionTask.run = function(taskName, info, options) {
    
    this.showVersion(taskName, info, options);
  };

module.exports = VersionTask;   
