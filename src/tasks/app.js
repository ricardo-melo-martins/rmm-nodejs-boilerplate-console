const { prompt } = require('inquirer')
//   const version = args.package_version || (await prompt([{ name: 'version', message: 'Initial version:', default: '0.1.0', validate: v => v.match(/[0-9]+\.[0-9]+\.[0-9]+/) !== null }])).version


var AppTask = {};
  
AppTask.showApp = function(taskName, info, options) {
  console.log(`run ${taskName} task`, info, options)
}

AppTask.run = function(taskName, info, options) {
  
  this.showApp(taskName, info, options);
};

module.exports = AppTask;   
